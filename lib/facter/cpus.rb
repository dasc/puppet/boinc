# cpus.rb

Facter.add("physicalprocessorcorecount") do
	confine :kernel => 'Linux'
        setcode do
                %x{lscpu -p | grep -v '^#' | cut -f 2 -d , | sort -nu | wc -l}.chomp.to_i
        end
end
