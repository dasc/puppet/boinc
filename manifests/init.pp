class boinc (
    String $package_ensure,
    String $service_ensure,
    String $workdir,
    Boolean $service_enable,
    Boolean $service_managed,
) {
    package { 'boinc-client': ensure => $package_ensure, }

    file {'/var/lib/boinc/fetch_work.sh':
	ensure => present,
	owner => 'boinc',
	group => 'boinc',
	mode => '0755',
	seltype => 'boinc_var_lib_t',
	source => [ "puppet://puppet/modules/boinc/var/lib/boinc/fetch_work.sh" ],
	require => File['/var/lib/boinc'],
    }

    file {'/var/lib/boinc/cc_config.xml':
	ensure => 'present',
	owner => 'boinc',
	group => 'boinc',
	mode => '0644',
	seltype => 'boinc_var_lib_t',
	content => epp('boinc/cc_config.xml.epp'),
	require => File['/var/lib/boinc'],
    }
    file {'/var/lib/boinc/account_einstein.phys.uwm.edu.xml':
	ensure => 'present',
	owner => 'boinc',
	group => 'boinc',
	mode => '0644',
	seltype => 'boinc_var_lib_t',
	source => [ "puppet://puppet/modules/boinc/var/lib/boinc/account_einstein.phys.uwm.edu.xml" ],
	require => File['/var/lib/boinc'],
    }

    exec { '/bin/chown -R boinc:boinc /var/lib/boinc/':
	refreshonly => true,
    }

    if $service_managed {
	service {'boinc-client':
	    enable => $service_enable,
	    ensure => $service_ensure,
	}
    }

    file {'/etc/systemd/system/boinc-client.service.d/50-CPUShares.conf':
	ensure => 'file',
	owner => 'root',
	group => 'root',
	mode => '0644',
	source => [ "puppet://puppet/modules/boinc/etc/systemd/system/boinc-client.service.d/50-CPUShares.conf" ],
    }
    file {'/etc/systemd/system/boinc-client.service.d/10-oom.conf':
	ensure => 'file',
	owner => 'root',
	group => 'root',
	mode => '0644',
	source => [ "puppet://puppet/modules/boinc/etc/systemd/system/boinc-client.service.d/10-oom.conf" ],
    }
    file {'/etc/systemd/system/boinc-client.service.d':
	ensure => 'directory',
	owner => 'root',
	group => 'root',
	mode => '0755',
    }
    Selinux::Semodule {'local_boinc':
        ensure => 'present',
        source => "puppet://puppet/modules/boinc/usr/share/selinux/targeted/local_boinc.pp",
    }

    exec { '/usr/sbin/semanage fcontext -a -e /var/lib/boinc boinc_work_dir':
        command => "/usr/sbin/semanage fcontext -a -e /var/lib/boinc ${workdir}",
	returns => [0, 1],
        refreshonly => true,
    } ->
    exec { '/usr/sbin/restorecon -R boinc_work_dir':
        command => "/usr/sbin/restorecon -R ${workdir}/",
	subscribe => [
	    Selinux::Semodule['local_boinc'],
	],
	refreshonly => true,
    }
    if $workdir == '/var/lib/boinc' {
	file { "/var/lib/boinc":
	    ensure => 'directory',
	    owner => 'boinc',
	    group => 'boinc',
	    mode => '0755',
	    seltype => 'boinc_var_lib_t',
	    require => Package['boinc-client'],
	    notify => Exec['/usr/sbin/restorecon -R boinc_work_dir']
	}
    } else {
	file { "${workdir}":
	    ensure => 'directory',
	    owner => 'boinc',
	    group => 'boinc',
	    mode => '0755',
	    seltype => 'boinc_var_lib_t',
	    require => Package['boinc-client'],
	    notify => [
		Exec['/usr/sbin/semanage fcontext -a -e /var/lib/boinc boinc_work_dir'],
		Exec['/usr/sbin/restorecon -R boinc_work_dir'],
	    ],
	}
	file { '/var/lib/boinc':
	    ensure => 'link',
	    force => true,
	    target => "${workdir}",
	    seltype => 'boinc_var_lib_t',
	    require => Package['boinc-client'],
	    notify => Exec['/bin/chown -R boinc:boinc /var/lib/boinc/'],
	}
    }
}
