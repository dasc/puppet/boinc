#!/bin/sh

# read the machine ClassAd and extract what we need
# (set _CONDOR_BOINC_SLOT so that BOINC config variables can
#  reference $(BOINC_SLOT))

# standard input is the ClassAd of the slot that is looking for work
# this is how we get the _CONDOR_BOINC_SLOT
#Some Condor tools utilize environment variables to set their configuration. These tools search for specifically-named environment variables. The variables are prefixed by the string _CONDOR_ or _condor_. The tools strip off the prefix, and utilize what remains as configuration. As the use of environment variables is the last within the ordered evaluation, the environment variable definition is used. The security of the system is not compromised, as only specific variables are considered for definition in this manner, not any environment variables with the _CONDOR_ prefix


eval `awk '/^SlotID/ {print "export _CONDOR_BOINC_SLOT="$3}'`

# load the following config variables from the condor configuration
BOINC_ConfigVars="
BOINC_Executable
BOINC_InitialDir
BOINC_Owner
BOINC_User
BOINC_Arguments
BOINC_Output
BOINC_Error
BOINC_Requirements
"
for var in ${BOINC_ConfigVars}; do
  value=`condor_config_val $var`

  # if anything is not defined, bail out
  if [ "$?" != 0 ] || [ "$value" = "" ]; then
    echo "Failed to look up $var in condor configuration." 2>&1
    exit 1
  fi

  eval $var=\'$value\'
done

#if [ ! -d ${BOINC_InitialDir} ]
#then
    #mkdir -p ${BOINC_InitialDir}
    #cp /usr1/BOINC/account_einstein.phys.uwm.edu.xml ${BOINC_InitialDir}
    #cp /usr1/BOINC/cc_config.xml ${BOINC_InitialDir}
#if

# sleep a bit so that the two boinc processes don't interfere with each other when getting work from the scheduler
# is this a problem?

sleeptime=10
sleep $sleeptime

# generate the BOINC job ClassAd
echo "Cmd = \"${BOINC_Executable}\""
echo "IWD = \"${BOINC_InitialDir}\""
echo "Owner = \"${BOINC_Owner}\""
echo "User = \"${BOINC_User}\""
echo "JobUniverse = 5"
echo "Arguments = \"${BOINC_Arguments}\""
echo "Out = \"${BOINC_Output}\""
echo "Err = \"${BOINC_Error}\""
echo "NiceUser = TRUE"
echo "ImageSize = 1"
echo "Requirements = ${BOINC_Requirements}"
echo "MaxJobRetirementTime = 0"
echo "JobLeaseDuration = 604800"
